
from paramiko import SSHClient , AutoAddPolicy, Channel ,SFTPClient
import paramiko 
import datetime
import os
import time

from config import *

class Connect_ssh:

    def __init__(self):
        self.ssh_connect = None
        self.sftp_connect = None
        self.host = None
        self.user = None


    def target_host(self):
        hosts = []
        path = os.path.abspath("list_ip.txt")
        
        if os.path.exists(path) == True:
            f = open(path , "r" )

            ips = f.readlines()

            if len(ips) == 1:
                hosts = ips
               
            else:
                for ip in ips:
        
                    if ip[-1] == '\n':
                        ip = ip[:-1]
                        if ip[-1] != "\n":
                            if ip[-1] != '':
                                hosts.append(ip.strip())          
            f.close()

            return hosts

        else:
            return "No se encuentra el archivo list_ip.txt"

    #-------- connect ------------


    def connect(self,host,user,password=None,path_ssh_key=None):
        self.host = host
        self.user = user
        self.ssh_connect = SSHClient()
        self.ssh_connect.set_missing_host_key_policy(AutoAddPolicy())

        

        if DEBUG is True:
            paramiko.common.logging.basicConfig(level=paramiko.common.DEBUG)
            
        try:
            self.ssh_connect.connect(
                            hostname = host,
                            username = user,
                            password = password,
                            look_for_keys= True,
                            key_filename = path_ssh_key,
                            timeout= 500
                            )
           
        except:
            return 'Could not connect'
        finally:
            return self.ssh_connect



    def close_connection(self):

        self.ssh_connect.close()

                                            
    def execute_command(self ,ssh_connect,list_command):

        outputs=''
        

        result = open( "result.logs" ,'a',encoding='utf-8')

        if self.ssh_connect is None:
            self.ssh_connect = self.connect(self.host,self.user)

        channel = self.ssh_connect.invoke_shell()

        
        for i,command in enumerate(list_command):

            channel.send(f'{command}\r ')
            time.sleep(2)
    
        while True :
            if channel.recv_ready():
                out = channel.recv(9999)
                outputs += out.decode("utf-8")
                time.sleep(3)
                
                channel.send('\x03')
                break
        
        if console is True:

            print(outputs)
        else:
            result.write(f' server  | {self.host} | : INFO :\n {outputs}\n --------------------_*_*_E N D_*_*_--------------------\n ')

            result.tell()

        channel.close()
        result.close()

